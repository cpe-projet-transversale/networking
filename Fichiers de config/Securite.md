# Bannière d'avertissement (Conditions Juridique)
banner motd 'Acces aux utilisateurs autorises uniquement !! '

# Mot de passe utilisant le Scrypt Hashing algorithm
enable algorithm-type scrypt secret 9 ciscomdp#

# Mot de passe min 10 caractères
security passwords min-lenght 10 

# Mot de passe hasher en MD5 par défaut
service password-encryption 

line vty 0 10
# Authentification avec la base de donnée locale 
login local

# Fermeture de sessions SSH/Telnet après 3min30 d'innactivité 
exec-timeout 3 30

# Protocol SSH supporté uniquement
transport input ssh 

line console 0 
login local
exec-timeout 3 30

# Empêche de se log pendant 15s après 5 tentatives infructueuses en 60s. (Protection anti Dos)
login block-for 15 attempts 5 within 60

# Identifier les hosts autorisés à se connecter sur le routeur

ip access-list standard PERMIT-ADMIN
remark Uniquement les Administrateurs sont autorisés
permit 164.4.0.50
exit

login quiet-mode access-class PERMIT-ADMIN

# Attendre 10s après une connexion infructueuse. (Anti dictionary attacks)
login delay 10

# Génération Syslog
login on-success log 
login on-failure log 

# Le nombre de connexion infructueuses autorisées
security authentication failure rate 3 log 
## show login

# Mettre en place SSH

ip domain-name ~domain-name.com~
crypto key generate rsa general-keys modulus 1024
ip ssh version 2
## show crypto key mypubkey rsa

# Supprimer la clé RSA 
crypto key zeroize rsa

# Interval de temps que tu dois attendre poru authentifier un autre user
ip ssh time-out 60

# Tentative infructueuse avant d'être déconnecté
ip ssh authentication-retries 2

## show ip ssh

# Niveau de privilège
!Level 5 and SUPPORT user configuration 
privilege exec level 5 ping 
enable algorithm-type scrypt secret level 5 cisco5
username SUPPORT privilege 5 algorithm-type scrypt secret cisco5

!Level 10 and JR-ADMIN user configuration
privilege exec level 10 reload 
enable algorithm-type scrypt secret level 10 cisco10 
username JR-ADMIN privilege 10 algorithm-type scrypt secret cisco 10

!Level 15 and ADMIN user configuration
enable algorithm-type scrypt secret level 10 cisco10 
username ADMIN privilege 15 algorithm-type scrypt secret cisco 123

# Se connecter en fonction du niveau de privilèges

enable 5 
<cisco5>
## Show privilege



