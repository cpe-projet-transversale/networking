# Stratégie Réseaux

## Configuration classique :

* Nom de l'équipement 
* Désactivation de recherche DNS
* Configuration des adresses IP des interfaces
* Routage OSPF, BGP (IBGP), BGP (EBGP)
* NAT-PAT
* DHCP


## QOS :

* Fichier Host
* Load Balancing
* QOS tag des trames
* HSRP


## Sécurité 

* ACL 
* Port security
* Authentification 
* Chiffrement 
* Politique de mot de passe

## Règles d'adressage et de nommage

* IBGP : Dans les réseaux d'interconnexion des routeurs, le .1 est du côté du routeur avec l'ID le plus faible. 
  Ex : lien entre R1 et R2 : x.x.x.1 côté R1, x.x.x.2 côté R2

[Voir ici](https://gitlab.com/cpe-projet-transversale/networking/uploads/c568b73f2bd66c5764a703ca1eca539f/Capture_du_2019-12-02_15_03_57.png)

* Les réseaux connectés via l'AS 100 suivront l'adressage suivant : 172.16.x00.x avec le premier x représentant le numéro du réseaux dans l'ordre si dessous
* Le.1 est du côté du routeur extérieur

* LIAISON DATACENTER 172.16.100.x 
* LIAISON SDIS-1 172.16.200.x
* LIAISON SDIS-2 172.16.300.x

## Nomenclature Loopbacks

* Pour les routeurs des SDIS et du Datacenter, les loopback vont être adressées comme ceci : 
* LOOPBACK DATACENTER : 192.168.100.1
* LOOPBACK SDIS-1 : 192.168.150.1
* LOOPBACK SDIS-2 : 192.168.200.1


## Nomenclature interfaces SDIS-1

* Interface SDIS-R1 coté AS 65001 164.4.2.252
* Interface SDIS-R2 coté AS 65001 164.4.2.253


## Règles par feu :

* 164.4.0.0 peut communiquer sur internet (sauf datacenter règle à part)
* Datacenter peut communiquer avec extérieur que si établie
* Sinon tous les autres sous réseaux peuvent communiquer avec le datacenter. Aussi bien interne que DMZ.
* Permit le ping des sous-r entre eux et vers le datacenter interne et dmz.


